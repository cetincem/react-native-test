import React, {useCallback, useEffect, useMemo, useRef, useState} from 'react';
import {
  FlatList,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {getPostById, getUserPosts} from '../api';
import {useStateValue} from '../global/context';
import {slowFunction} from '../global/helper';
import {IPost, IUser} from '../models/apiModels';

/**
 * Show all users saved in global context ordered by their ID
 * Delete user from global context when clicked on it
 * Optimize time wasted on slowFunction
 */

const UserComponent: React.FC<{user: IUser; onClick: any}> = ({
  user,
  onClick,
}) => {
  const slowResult = useMemo(() => slowFunction(user.name), [user.name]);
  console.log(`Render ${user.name}`);

  return (
    <View style={{marginLeft: 20, marginTop: 20}}>
      <TouchableOpacity onPress={() => onClick(user)}>
        <Text style={{fontWeight: 'bold'}}>
          {user.id}. {user.name}
        </Text>
        <Text>Email: {user.email} </Text>
        <Text style={{color: 'green'}}>Slow Result: {slowResult} </Text>
      </TouchableOpacity>
    </View>
  );
};

function UserPropsAreEqual(userA, userB) {
  return userA.id === userB.id;
}
const UserMemoComponent = React.memo(UserComponent, UserPropsAreEqual);

function Users({navigation}) {
  const [{allUsers}, dispatch] = useStateValue();

  const orderedUsers: IUser[] = [...allUsers].sort((a,b) => a.id - b.id);

  const deleteUser = useCallback((user: IUser) => {
    dispatch({
      type: 'user/delete',
      user,
    });
  }, []);

  return (
    <View>
      <FlatList
        data={orderedUsers}
        renderItem={({item}) => <UserMemoComponent user={item} onClick={deleteUser} />}
        keyExtractor={item => item.id.toString()}
      />
    </View>
  );
}


export default Users;
